<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'u24600p34830_wp2' );

/** MySQL database username */
define( 'DB_USER', 'u24600p34830_wp2' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Y*)vCkw#uRD)FACBFu[95]*0' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'TQnU0NpR45kWHiZSnUjLDXyVrZr0PW7HlHcN4I8yOJvd2GEhNBq96DYsbtRBFIiY');
define('SECURE_AUTH_KEY',  'ZhcV7Ac8y5H2W7v5Bfsg7ZEb3ctgjkgjISTnU5Cm40rEOJ02NJbHpr8SVjLpDw2R');
define('LOGGED_IN_KEY',    '0E29lvupq3tM7jx95B5yhQrRYlbxrby8aV8NbOkxo4EdMQ9do69KGx7TZabAqxUf');
define('NONCE_KEY',        'F2tuaiiEXWjHsbjvzubTqYpUyKTLo1lxWDcadRdoeZOFM9vjoYzyIgeXUnSlo4hz');
define('AUTH_SALT',        'ob0f3Hdxcrw1Up6RMO2p841UqPS8b6d2MTO6vVYLYSPwceBALnj4rt7uqfs1SMlO');
define('SECURE_AUTH_SALT', 'i8d1VDLdE1jbVjf6qnACknoDl53Gh9jIYZ4CpX8WLqOUDIoKnErnDhpnoi0PJRyq');
define('LOGGED_IN_SALT',   'tKipKsd23JdwKGeUOWUrKvX55IlMv0MOga0RlZepXrEl2fqYBr29XbaNuPQJ3Iqq');
define('NONCE_SALT',       'oTz5nXMS321ZIDKrSRPCtDoR1Jgb12QE0BDmgkoQd0ZCWticVs0gvyDK4t8SXOaO');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');
define('FS_CHMOD_DIR',0755);
define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed externally by Installatron.
 * If you remove this define() to re-enable WordPress's automatic background updating
 * then it's advised to disable auto-updating in Installatron.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
